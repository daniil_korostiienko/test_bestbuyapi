var express = require('express');
var request = require('request');
var path = require('path');
var logger = require('morgan');
var favicon = require('serve-favicon');
var bodyParser = require('body-parser');

var app = express();

app.set('view engine', 'html');
app.use(favicon(__dirname + '/public/images/favicon.ico'));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function (request, response) {
  response.sendFile('index.html', { root: __dirname });
});

app.listen(3000);

console.log("\nServer start on 127.0.0.1:3000\n");

app.get('/data*', function(req, res) {
  var options = req.query;
  request.get('http://api.remix.bestbuy.com/v1/categories?show=id,name&format=json&pageSize=10&apiKey=cn38skdtns9456vrtnf4rhdf&page=' + options.page).pipe(res);
});

app.get('/category/*', function(req, res) {
  var options = req.query;
  console.log('Received Categories from Category: ' + options.categoryId + ' on page ' + options.page);
  request.get('http://api.remix.bestbuy.com/v1/categories(id=' + options.categoryId + ')?format=json&pageSize=10&apiKey=cn38skdtns9456vrtnf4rhdf&show=subCategories').pipe(res);
});

app.get('/products/*', function(req, res) {
  var options = req.query;
  console.log('Received Products from Category: ' + options.categoryId + ' on page ' + options.page);
  request.get('http://api.remix.bestbuy.com/v1/products(categoryPath.id=' + options.categoryId +
  ')?apiKey=cn38skdtns9456vrtnf4rhdf&pageSize=10&format=json&show=sku,name,productId,thumbnailImage,regularPrice&page=' + options.page).pipe(res);
});

app.get('/search/*', function(req, res) {
  var options = req.query;
  console.log('Received Search query: ' + options.name + ' on page ' + options.page);
  request.get('http://api.remix.bestbuy.com/v1/products(name=' + options.name + '*|longDescription=' + options.name +
  '*)?apiKey=cn38skdtns9456vrtnf4rhdf&pageSize=10&format=json&show=sku,name,productId,thumbnailImage&page=' + options.page).pipe(res);
});

app.get('/product/:sku', function(req, res) {
  console.log('Received Product: ' + req.params.sku);
  request.get('http://api.remix.bestbuy.com/v1/products(sku=' + req.params.sku + ')?apiKey=cn38skdtns9456vrtnf4rhdf&format=json&show=all').pipe(res);
});

app.get('/reviews/:sku', function(req, res) {
  console.log('Received Reviews for Product: ' + req.params.sku);
  request.get('http://api.remix.bestbuy.com/v1/reviews(sku=' + req.params.sku + ')?format=json&apiKey=cn38skdtns9456vrtnf4rhdf&show=all').pipe(res);
});

app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  console.log('error ' + req.url);
});

app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  console.log(err.message + ' ' + req.url);
});

module.exports = app;