myApp.service('cartStorage', function($rootScope) {
  var self = this,
      phones = [];

	this.getPhones = function () {
    return phones;
  };

  this.getPrice = function () {
    var price = 0;
     _.each(phones, function (phone) {
      price += phone.price;
    });

    return price;
  };

  this.changePrice = function(price) {
    $rootScope.$broadcast("priceChanged", price);
  };

  this.addPhone = function (phone) {
    phones.push(phone);
    self.changePrice(phone.price);
    $rootScope.$broadcast("phoneAdded", phone);
  };

  this.removePhone = function (index) {
    self.changePrice(-phones[index].price);
    phones.splice(index, 1);
    $rootScope.$broadcast("phoneDeleted", index);
  };
});
