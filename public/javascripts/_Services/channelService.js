myApp.service('channelService', function($rootScope, $location, $http) {
	var category,
	    categories = {},
	    products = {},
	    currentUrl = '/data?page=';

  this.getCategories = function (page) {
    $http.get('/data?page=' + page).success(function(data, status, headers, config) {
      categories = data;
      $rootScope.$broadcast("categoriesLoaded", categories);
    });
    currentUrl = '/data?page=';
  };

  this.getData = function (type, index, page) {
    var types = {
          'category': function () {
            $http.get('/category/?categoryId=' + index + '&page=1').success(function(data, status, headers, config) {
              if (!data.categories[0].subCategories.length) {
                types['products']();
              } else {
                currentUrl = '/category/?categoryId=' + index + '&page=';
                category = data;
                $rootScope.$broadcast("categoryLoaded", category);
              };
            });
          },

          'products': function () {
            $http.get('/products/?categoryId=' + index + '&page=1').success(function(data, status, headers, config) {
              currentUrl = '/products/?categoryId=' + index + '&page=';
              products = data;
              $rootScope.$broadcast("productsLoaded", products);
            });
          },

          'product': function () {
            $http.get('/product/' + index).success(function(data, status, headers, config) {
              product = data;
                $http.get('/reviews/' + index).success(function(data, status, headers, config) {
                  reviews = data;
                  $rootScope.$broadcast("reviewsLoaded", reviews);
                });
              $rootScope.$broadcast("productLoaded", product);
            });


          }
        };

    types[type]();
  };

  this.setPage = function (page) {
    $http.get(currentUrl + page).success(function(data, status, headers, config) {
      var url = currentUrl.split('/'),
          types = {
            'data': function () {
              $rootScope.$broadcast("categoriesLoaded", data);
            },

            'category': function () {
              $rootScope.$broadcast("categoryLoaded", data);
            },

            'products': function () {
              $rootScope.$broadcast("productsLoaded", data);
            },

            'search': function () {
              $rootScope.$broadcast("searchDone", data);
            }
        };

      url = url[1].split('?');
      types[url[0]]();
    });
  };

  this.searchProducts = function (name) {
    $http.get('/search/?name=' + name + '&page=1').success(function(data, status, headers, config) {
      currentUrl = '/search/?name=' + name + '&page=';
      products = data;
      $rootScope.$broadcast("searchDone", products);
    });
  };
});