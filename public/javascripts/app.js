'use strict';

var myApp = angular.module('myApp', [
  'ui.bootstrap',
  'ui.router',
  'ngAnimate',
  'myApp.home'
])

.config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise("/");

	$stateProvider
		.state('Home', {
      url: '/*path',
      templateUrl: 'javascripts/Home/views/home.html',
      controller: 'HomeCtrl'
    });
});
