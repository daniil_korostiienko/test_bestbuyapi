'use strict';

angular.module('myApp.home')

.directive('item', function() {
  return {
    restrict: 'A',
    transclude: true,
    templateUrl: 'javascripts/Home/views/item.html'
  };
});