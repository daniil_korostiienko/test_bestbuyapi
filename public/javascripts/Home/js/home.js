'use strict';

angular.module('myApp.home', [])

.controller('HomeCtrl', ['$scope', 'channelService', '$stateParams', '$location', function($scope, channelService, $stateParams, $location) {
	var unbindCategoriesLoaded = $scope.$on("categoriesLoaded", handleCategoriesLoaded),
	    unbindCategoryLoaded = $scope.$on("categoryLoaded", handleCategoryLoaded),
	    unbindProductsLoaded = $scope.$on("productsLoaded", handleProductsLoaded),
	    unbindProductLoaded = $scope.$on("productLoaded", handleProductLoaded),
	    unbindReviewsLoaded = $scope.$on("reviewsLoaded", handleReviewsLoaded),
	    unbindSearchDone = $scope.$on("searchDone", handleSearchDone),
	    typeHash = {
	      category: 'category',
	      products: 'products',
        product: 'product',
	    };

  $scope.search = function (event, value) {
    if (event.keyCode == 8 && !value) {
      channelService.getCategories(1);
    } else {
      channelService.searchProducts(value);
    };
  };

  $scope.info = {};
  $scope.info.currentPage = 1;

  $scope.paths = $stateParams.path.split('/');
  $scope.paths = $scope.paths.slice(0, $scope.paths.length - 1);

  if ($scope.paths.length > 0) {
    if ($scope.paths[$scope.paths.length - 1][0] > 0) {
      channelService.getData('product', $scope.paths[$scope.paths.length - 1]);
    } else {
      channelService.getData('category', $scope.paths[$scope.paths.length - 1]);
    };
  } else {
    channelService.getCategories($scope.info.currentPage);
  };

	function handleCategoriesLoaded (event, data) {
	  $scope.info = data;
		$scope.items = data.categories;
		$scope.type = typeHash.category;
	};

  function handleCategoryLoaded (event, data) {
		$scope.items = data.categories[0].subCategories;
		$scope.type = typeHash.category;
	};

	function handleProductsLoaded (event, data) {
    $scope.info = data;
  	$scope.items = data.products;
  	$scope.type = typeHash.product;
  };

  function handleProductLoaded (event, data) {
    $scope.info = data;
    $scope.items = data.products;
    $scope.type = typeHash.product;
  };

  function handleReviewsLoaded (event, data) {
    $scope.items[0].reviews = data.reviews;
  };
  function handleSearchDone (event, data) {
    $scope.info = data;
    $scope.items = data.products;
    $scope.type = typeHash.product;
  };

  $scope.changeState = function (type, id, page) {
    channelService.getData(type, id);
    $location.url($location.path() + id + '/');
  };

  $scope.pageChanged = function () {
    channelService.setPage($scope.info.currentPage);
  };

}]);